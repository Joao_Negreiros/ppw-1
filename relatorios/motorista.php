<?php
include("../conecta.php");
mysqli_set_charset($conexao, "utf8");

$sql = "select * from motoristas;";
$query = mysqli_query($conexao, $sql);


?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<title>Consultar Motorista</title>
		<link rel="stylesheet" href="../framework/css/skeleton.css">
		<link rel="stylesheet" href="../framework/css/normalize.css">

		<style>
		
		body{
			padding: 3px;
		}

		table, th, td {
  		border: 1px solid black;
  		padding: 3px;
		}

		</style>
	</head>
	<body>
		
		<h1>Motoristas</h1>             

		<table class="u-full-width" id="tabela">
			
			<thead>
				
				<tr>
					<td >Código do Motorista</td>
					<td>Nome</td>
					<td>Endereço</td>
					<td>Número</td>
					<td>Bairro</td>
					<td>Cidade</td>
					<td>Estado</td>
					<td>CEP</td>
					<td>Telefone</td>
					<td>Data de Nascimento</td>
					<td>Data de Vencimento da CNH</td>
					<td>Tipo da CNH</td>
				</tr><?php while($dado = $query->fetch_array()) { ?>

				<tr>
					<td><?php echo$dado["cod_motorista"] ?></td>
					<td><?php echo$dado["nome"] ?></td>
					<td><?php echo$dado["endereco"] ?></td>
					<td><?php echo$dado["numero"] ?></td>
					<td><?php echo$dado["bairro"] ?></td>
					<td><?php echo$dado["cidade"] ?></td>
					<td><?php echo$dado["estado"] ?></td>
					<td><?php echo$dado["cep"] ?></td>
					<td><?php echo$dado["fone"] ?></td>
					<td><?php echo$dado["nascimento"] ?></td>
					<td><?php echo$dado["vencimento_cnh"] ?></td>
					<td><?php echo$dado["tipo_cnh"] ?></td>
				</tr><?php } ?>

			</thead>

		</table>
		
		<a href="../painel.php" class="button button-primary">Voltar</a>
		<a href="" class="button button-primary" onclick="imprime()">Imprimir</a>

		<script>
			var tabela = document.getElementById('tabela').value;
			
			function imprime (table){
				text = tabela
				print(text)
			}

		</script>

	</body>
</html>