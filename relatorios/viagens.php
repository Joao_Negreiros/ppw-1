<?php
include("../conecta.php");
mysqli_set_charset($conexao, "utf8");

$sql = "select * from viagens";
$query = mysqli_query($conexao, $sql);

?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<title>Dados das Viagens</title>
		<link rel="stylesheet" href="../framework/css/skeleton.css">
		<link rel="stylesheet" href="../framework/css/normalize.css">

		<style>
		
		body{
			padding: 3px;
		}

		table, th, td {
  		border: 1px solid black;
  		padding: 3px;
		}

		</style>
	</head>
	<body>
		
		<h1>Dados das Viagens</h1>

		<table class="u-full-width">
			
			<thead>
				
				<tr>
					<td>Data da Viagem</td>
					<td>Rota</td>
					<td>Placa do Veículo</td>
					<td>Motorista</td>

				</tr><?php while($dado = $query->fetch_array()) { ?>

				<tr>
					<td><?php echo$dado["data_viagem"] ?></td>
					<td><?php echo$dado["rota"] ?></td>
					<td><?php echo$dado["placa"] ?></td>
					<td><?php echo$dado["motorista"] ?></td>
				</tr><?php } ?>

			</thead>

		</table>
		
		<a href="../painel.php" class="button button-primary">Voltar</a>
		<a href="" class="button button-primary" onclick="imprime()">Imprimir</a>

		<script>
			var tabela = document.getElementById('tabela').value;
			
			function imprime (table){
				text = tabela
				print(text)
			}

		</script>

	</body>
</html>