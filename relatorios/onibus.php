<?php
include("../conecta.php");
mysqli_set_charset($conexao, "utf8");

$sql = "select * from onibus";
$query = mysqli_query($conexao, $sql);

?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<title>Consultar Ônibus</title>
		<link rel="stylesheet" href="../framework/css/skeleton.css">
		<link rel="stylesheet" href="../framework/css/normalize.css">

		<style>
		
		body{
			padding: 3px;
		}

		table, th, td {
  		border: 1px solid black;
  		padding: 3px;
		}

		</style>
	</head>
	<body>
		
		<h1>Dados do Ônibus</h1>

		<table class="u-full-width">
			
			<thead>
				
				<tr>
					<td>Placa</td>
					<td>Marca</td>
					<td>Tipo de Veículo</td>
					<td>Ano de Fabricação</td>
					<td>Observação</td>
					<td>Número de Assentos</td>
				</tr><?php while($dado = $query->fetch_array()) { ?>

				<tr>
					<td><?php echo$dado["placa"] ?></td>
					<td><?php echo$dado["marca"] ?></td>
					<td><?php echo$dado["tipo_veiculo"] ?></td>
					<td><?php echo$dado["ano_fabricacao"] ?></td>
					<td><?php echo$dado["observacao"] ?></td>
					<td><?php echo$dado["numero_assentos"] ?></td>
				</tr><?php } ?>

			</thead>

		</table>
		
		<a href="../painel.php" class="button button-primary">Voltar</a>
		<a href="" class="button button-primary" onclick="imprime()">Imprimir</a>

		<script>
			var tabela = document.getElementById('tabela').value;
			
			function imprime (table){
				text = tabela
				print(text)
			}

		</script>

	</body>
</html>