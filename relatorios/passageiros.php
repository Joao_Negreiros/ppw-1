<?php
include("../conecta.php");
mysqli_set_charset($conexao, "utf8");

$sql = "select * from passageiros ;";
$query = mysqli_query($conexao, $sql);
$row = mysqli_num_rows($query);

?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<title>Passageiros</title>
		<link rel="stylesheet" href="../framework/css/skeleton.css">
		<link rel="stylesheet" href="../framework/css/normalize.css">

		<style>
		
		body{
			padding: 3px;
		}

		table, th, td {
  		border: 1px solid black;
  		padding: 3px;
		}

		</style>
	</head>
	<body>
		
		<h1>Dados do Passageiro</h1>

		<table class="u-full-width">
			
			<thead>
				
				<tr>
					<td >CPF do Passageiro</td>
					<td>Nome</td>
					<td>Cidade</td>
					<td>Estado</td>
					<td>Data de Nascimento</td>
					<td>Observação</td>
				</tr><?php while($dado = $query->fetch_array()) { ?>

				<tr>
					<td><?php echo$dado["cpf"] ?></td>
					<td><?php echo$dado["nome"] ?></td>
					<td><?php echo$dado["cidade"] ?></td>
					<td><?php echo$dado["estado"] ?></td>
					<td><?php echo$dado["nascimento"] ?></td>
					<td><?php echo$dado["observacao"] ?></td>

				</tr><?php } ?>

			</thead>

		</table>
		
		<a href="../painel.php" class="button button-primary">Voltar</a>
		<a href="" class="button button-primary" onclick="imprime()">Imprimir</a>

		<script>
			var tabela = document.getElementById('tabela').value;
			
			function imprime (table){
				text = tabela
				print(text)
			}

		</script>

	</body>
</html>