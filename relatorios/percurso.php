<?php
include("../conecta.php");
mysqli_set_charset($conexao, "utf8");

$sql = "select * from percurso;";
$query = mysqli_query($conexao, $sql);

?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<title>Consultar Percurso</title>
		<link rel="stylesheet" href="../framework/css/skeleton.css">
		<link rel="stylesheet" href="../framework/css/normalize.css">

		<style>
		
		body{
			padding: 3px;
		}

		table, th, td {
  		border: 1px solid black;
  		padding: 3px;
		}

		</style>
	</head>
	<body>
		
		<h1>Dados do Percurso</h1>

		<table class="u-full-width">
			
			<thead>
				
				<tr>
					<td>Rota</td>
					<td>Duração</td>
					<td>Necessário Troca de Motorista</td>
					<td>Quantidade de Trocas</td>
					<td>Liberado Para Venda</td>
					<td>Preço da Passagem Normal</td>
					<td>Preço da Passagem com Bagagem</td>
				</tr><?php while($dado = $query->fetch_array()) { ?>

				<tr>
					<td><?php echo$dado["rota"] ?></td>
					<td><?php echo$dado["duracao"] ?></td>
					<td><?php echo$dado["troca_motorista"] ?></td>
					<td><?php echo$dado["quantos_motoristas"] ?></td>
					<td><?php echo$dado["liberado_venda"] ?></td>
					<td><?php echo$dado["valor_passagem_sem_bagagem"] ?></td>
					<td><?php echo$dado["valor_passagem_com_bagagem"] ?></td>
				</tr><?php } ?>

			</thead>

		</table>
		
		<a href="../painel.php" class="button button-primary">Voltar</a>
		<a href="" class="button button-primary" onclick="imprime()">Imprimir</a>

		<script>
			var tabela = document.getElementById('tabela').value;
			
			function imprime (table){
				text = tabela
				print(text)
			}

		</script>

	</body>
</html>