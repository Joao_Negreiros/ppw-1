<?php
include("../conecta.php"); //Arquivo de conexão
$rota = mysqli_real_escape_string($conexao, $_POST['rota']); // Variável que vem do formulário HTML

$sql = "select * from percurso where rota = '$rota';"; // Instrução para confirmar a existência do percurso
$query = mysqli_query($conexao, $sql); // Execução da instrução acima
$row = mysqli_num_rows($query); // Número de linhas retornados pela intrução

if ($row == 0) // se não retornar nenhuma linha quer dizer que o percurso não existe
{
	echo '<h1>Percurso Não Encontrado</h1>';
	header('refresh:2;url=percurso.php');
	exit();
}

else
{

	$sql = "delete from percurso where rota = '$rota';"; // Instrução para deletar o percurso
	$query = mysqli_query($conexao, $sql); // Execução da instrução acima

	if ($query) // Caso seja executado com sucesso  
	{
		echo "<h1>Percurso Excluido com Sucesso</h1>";
		header('refresh:2;url=percurso.php');
	}
	else
	{
		echo "<h1>Não foi possível deletar o percurso</h1>";
		header('refresh:2;url=percurso.php');
	}

}
?>