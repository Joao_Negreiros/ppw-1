<?php
include("../conecta.php"); //Arquivo de conexão
$placa = mysqli_real_escape_string($conexao, $_POST['placa']); // Variável que vem do formulário HTML

$sql = "select * from onibus where placa = '$placa';"; // Instrução para confirmar a existência do ônibus
$query = mysqli_query($conexao, $sql); // Execução da instrução acima
$row = mysqli_num_rows($query); // Número de linhas retornados pela intrução

if ($row == 0) // se não retornar nenhuma linha quer dizer que o ônibus não existe
{
	echo '<h1>Ônibus Não Encontrado</h1>';
	header('refresh:2;url=onibus.php');
	exit();
}

else
{

	$sql = "delete from onibus where placa = '$placa';"; // Instrução para deletar o ônibus
	$query = mysqli_query($conexao, $sql); // Execução da instrução acima

	if ($query) // Caso seja executado com sucesso  
	{
		echo "<h1>Ônibus Excluido com Sucesso</h1>";
		header('refresh:2;url=onibus.php');
	}
	else
	{
		echo "<h1>Não foi possível deletar o ônibus</h1>";
		header('refresh:2;url=onibus.php');
	}

}
?>