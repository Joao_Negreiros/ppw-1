<?php
include("../conecta.php"); //Arquivo de conexão
$nome = mysqli_real_escape_string($conexao, $_POST['nome']); // Variável que vem do formulário HTML
$id = mysqli_real_escape_string($conexao, $_POST['id_motorista']); // Variável que vem do formulário HTML

$sql = "select * from motoristas where cod_motorista = '$id' and nome = '$nome' ;"; // Instrução para confirmar a existência do motorista
$query = mysqli_query($conexao, $sql); // Execução da instrução acima
$row = mysqli_num_rows($query); // Número de linhas retornados pela intrução

if ($row == 0) // se não retornar nenhuma linha quer dizer que o motorista não existe
{
	echo '<h1>Motorista Não Encontrado</h1>';
	header('refresh:2;url=motorista.php');
	exit();
}

else
{

	$sql = "delete from motoristas where cod_motorista = '$id' and nome = '$nome';"; // Instrução para deletar o motorista
	$query = mysqli_query($conexao, $sql); // Execução da instrução acima

	if ($query) // Caso seja executado com sucesso  
	{
		echo "<h1>Motorista Excluido com Sucesso</h1>";
		header('refresh:2;url=motorista.php');
	}
	else
	{
		echo "<h1>Não foi possível deletar o motorista</h1>";
		header('refresh:2;url=motorista.php');
	}

}
?>