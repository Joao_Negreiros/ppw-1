<?php
include("../conecta.php"); //Arquivo de conexão
$nome = mysqli_real_escape_string($conexao, $_POST['nome']); // Variável que vem do formulário HTML
$id = mysqli_real_escape_string($conexao, $_POST['id_usuario']); // Variável que vem do formulário HTML

$sql = "select * from usuarios where user_id = '$id' and nome_usuario = '$nome' ;"; // Instrução para confirmar a existência do usuario
$query = mysqli_query($conexao, $sql); // Execução da instrução acima
$row = mysqli_num_rows($query); // Número de linhas retornados pela intrução

if ($row == 0) // se não retornar nenhuma linha quer dizer que o usuario não existe
{
	echo '<h1>Usuário Não Encontrado</h1>';
	header('refresh:2;url=usuario.php');
	exit();
}

else
{

	$sql = "delete from usuarios where user_id = '$id' and nome_usuario = '$nome';"; // Instrução para deletar o usuario
	$query = mysqli_query($conexao, $sql); // Execução da instrução acima

	if ($query) // Caso seja executado com sucesso  
	{
		echo "<h1>Usuário Excluido com Sucesso</h1>";
		header('refresh:2;url=usuario.php');
	}
	else
	{
		echo "<h1>Não foi possível deletar o usuário</h1>";
		header('refresh:2;url=usuario.php');
	}

}
?>