<?php
include("../conecta.php"); //Arquivo de conexão
$nome = mysqli_real_escape_string($conexao, $_POST['nome']); // Variável que vem do formulário HTML
$cpf = mysqli_real_escape_string($conexao, $_POST['cpf_passageiro']); // Variável que vem do formulário HTML

$sql = "select * from passageiros where cpf = '$cpf' and nome = '$nome' ;"; // Instrução para confirmar a existência do passageiro
$query = mysqli_query($conexao, $sql); // Execução da instrução acima
$row = mysqli_num_rows($query); // Número de linhas retornados pela intrução

if ($row == 0) // se não retornar nenhuma linha quer dizer que o passageiro não existe
{
	echo '<h1>Passageiro Não Encontrado</h1>';
	header('refresh:2;url=passageiros.php');
	exit();
}

else
{

	$sql = "delete from passageiros where cpf = '$cpf' and nome = '$nome';"; // Instrução para deletar o passageiro
	$query = mysqli_query($conexao, $sql); // Execução da instrução acima

	if ($query) // Caso seja executado com sucesso  
	{
		echo "<h1>Passageiro Excluido com Sucesso</h1>";
		header('refresh:2;url=passageiros.php');
	}
	else
	{
		echo "<h1>Não foi possível deletar o passageiros</h1>";
		header('refresh:2;url=passageiros.php');
	}

}
?>