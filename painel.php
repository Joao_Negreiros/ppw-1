<?php   
session_start();
if (!($_SESSION['usuario']))
{
  header('Location: index.php');
}

?>
<!DOCTYPE html>
<html>
<head>
<title>Empresa de Ônibus</title>
<link rel="stylesheet" href="style.css">
</head>
<body>

<div class="header">
  <h1>Empresa de Ônibus</h1>
  <p>Projeto pré TCC da matéria PPW I.</p>
</div>

<div class="cabecalho">

        <ul>

          <li>

            <a href="" class="" style="color: white">Cadastrar</a>
            <ul>

              <li><a href="cadastros/motorista.php">Motoristas</a></li>
              <li><a href="cadastros/onibus.php">Ônibus</a></li>
              <li><a href="cadastros/passageiros.php">Passageiros</a></li>
              <li><a href="cadastros/percurso.php">Percurso</a></li>
              <li><a href="cadastros/usuario.php">Usuários</a></li>

            </ul>

          </li>

          <li>

            <a href="" class="" style="color: white">Lançamento</a>
            <ul>

              <li><a href="lançamentos/viagens.php">Viagens</a></li>
              <li><a href="lançamentos/vendas.php">Vendas de passagem</a></li>

            </ul>

          </li>

          <li>

            <a href="" class="" style="color: white">Relátorios</a>
            <ul>

              <li><a href="relatorios/motorista.php">Motoristas</a></li>
              <li><a href="relatorios/onibus.php">Ônibus</a></li>
              <li><a href="relatorios/passageiros.php">Pasageiros</a></li>
              <li><a href="relatorios/percurso.php">Percurso</a></li>
              <li><a href="relatorios/viagens.php">Viagens</a></li>

            </ul>

          </li>

          <li>

            <a href="" class="" style="color: white">Ajuda</a>
            <ul>

              <li><a href="database-backup.php">Back-up</a></li>
              <li><a href="">Restauração</a></li>
              <li><a href="ajuda/sobre.php">Sobre</a></li>

            </ul>

          </li>

          <a href="lougot.php" id = "sair" class="button button-primary" style="color: white">Sair</a>

        </ul>

      </div>


<div class="footer">
  <h2>© 2019 - 2º Etim Informática para Internet</h2>
</div>

</body>
</html>
