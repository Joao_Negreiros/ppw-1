<?php   
// Indentifica se o login foi efetuado
session_start();
if (!($_SESSION['usuario']))
{
  header('Location: ../index.php');
}

?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<title>Lançar Viangem</title>
		<link rel="stylesheet" href="../framework/css/skeleton.css">
		<link rel="stylesheet" href="../framework/css/normalize.css">

		<style>
			
			body{
				text-align: center;
			}
			.botao{
				text-align: left;
			}

		</style>

	</head>
	<body>
		
		<h1>Lançamento de Viagens</h1>
		<h6>*certifique-se de que os elementos que compõem a viagem existem (rota, motorista, etc.)</h6>

		<form style="margin: 1%;" method = "POST" action="lan_viagem.php">

	      <div class="row">

	        <div class="twelve columns"> <!-- Da o tamanho do grid 12 do framework -->
	              
	          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
	            <label for="exampleEmailInput">Data da Viagem:</label>
	            <input type="text" name="data" class="u-full-width" placeholder="AAAA-MM-DD">
	          </div>

	          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
	            <label for="exampleEmailInput">Rota:</label>
	            <input type="text" class="u-full-width" name = "rota" placeholder="(Origem – Destino) SOROCABA-MONGAGUÁ">
	          </div>

	        </div>

	        <div class="twelve columns"> <!-- Da o tamanho do grid 12 do framework -->

	          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
	            <label>Placa do Ônibus:</label>
	            <input type="text" name="placa" class="u-full-width" placeholder="XXX-0000">
	          </div>

	          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
	            <label for="exampleEmailInput">Motorista:</label>
	            <input type="text" name="motorista" class="u-full-width" placeholder="Número de identificação do motorista">
	          </div>

	        </div>

	      	<div class="botao">
	      		
				<input class="button-primary" type="submit" value="ENVIAR" style="margin-top: 1%"> <!-- botão para enviar o cadastro-->
	      		<a href="../painel.php" class="button button-primary">Voltar</a>  <!-- Botão para voltar ao painel -->

	      	</div>

	    </form>

	</body>
</html>