<?php
include("../conecta.php");  // Inclui o arquivo de conexão com o banco de dados
$nome = mysqli_real_escape_string($conexao, $_POST['nome']);  // Pega a variável que vem do formulário HTML
$cpf = mysqli_real_escape_string($conexao, $_POST['cpf_passageiro']);  // Pega a variável que vem do formulário HTML

$sql = "select * from passageiros where nome = '$nome' and cpf = '$cpf' ;"; // Instrução para confirmar a existência do passageiro
$query = mysqli_query($conexao, $sql); // Executa a instrução
$row = mysqli_num_rows($query); // Pega a quantidade de linhas retornadas pela instrução

if ($row == 0)  // Caso retorne 0 o passageiro não existe
{
  echo '<h1>Passageiro Não Encontrado</h1>';
  header('refresh:2;url=passageiros.php');
  exit();
}


?>

<!DOCTYPE html>
<html lang="pt-br">

  <head>
      <meta charset="UTF-8">
      <title>Cadatrar Passageiros</title>
      <link rel="stylesheet" type="text/css" href="../framework/css/skeleton.css">  <!-- Chama o css do framework -->
      <link rel="stylesheet" type="text/css" href="../framework/css/normalize.css">  <!-- Chama o css do framework -->
  </head>

  <body style="padding: 1%">
    
    <h1 style="margin: 1%">Cadastrar Passageiros</h1><?php while($dado = $query->fetch_array()) { ?>
  	<form style="margin: 1%;" method = "POST" action="alt_passageiros.php">   <!-- Formulário de cadastro -->
      <div class="row">

        <div class="twelve columns"> <!-- Da o tamanho do grid 12 do framework -->
              
          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Nome:</label>
            <input type="text" name="nome" class="u-full-width" value="<?php echo$dado["nome"] ?>">
          </div>

          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">CPF:</label>
            <input type="text" class="u-full-width" name="cpf" value="<?php echo$dado["cpf"] ?>">
          </div>
              
        </div>

        <div class="twelve columns"> <!-- Da o tamanho do grid 12 do framework -->

          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label>Cidade:</label>
            <input type="text" name="cidade" class="u-full-width" value="<?php echo$dado["cidade"] ?>">
          </div>

          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Estado:</label>
            <input type="text" name="estado" class="u-full-width" value="<?php echo$dado["estado"] ?>">
          </div>

        </div>

        <div class="twelve columns">
               	
          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Data de Nascimento:</label>
            <input type="text" name="nascimento" class="u-full-width" value="<?php echo$dado["nascimento"] ?>">
          </div>

          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Nota:</label> 
            <input type="text" name="obs" class="u-full-width" value="<?php echo$dado["observacao"] ?>">
          </div>

        </div>

      </div>

      <input class="button-primary" type="submit" value="Cadrastrar" style="margin: 1%"> <!-- botão para enviar o cadastro-->
      <a href="../cadastros/passageiros.php" class="button button-primary">Voltar</a>  <!-- Botão para voltar ao painel -->

    </form><?php } ?>

  </body>
</html>
