<?php
// Indentifica se o login foi efetuado   
session_start();
if (!($_SESSION['usuario']))
{
	header('Location: ../index.php');
}

?>


<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<title>Consultar Ônibus</title>
	    <link rel="stylesheet" type="text/css" href="../framework/css/skeleton.css"> <!-- Chama o arquivo css do framework -->
      	<link rel="stylesheet" type="text/css" href="../framework/css/normalize.css"> <!-- Chama o arquivo css do framework -->
	</head>
	<body style="padding: 1%">

        <div class="twelve columns"> <!-- Da o tamanho do grid 12 do framework -->
		
			<h1>Consultar Ônibus</h1>

        	<form action="onibus.php" method="POST">  <!-- Formulário de consulta -->
        	
				<div class="twelve columns"> <!-- Da o tamanho do grid 6 do framework -->
            		<label for="exampleEmailInput">Placa do Ônibus:</label> 
            		<input type="text" name="placa" class="u-full-width">
          		</div>
             
      			<input class="button-primary" type="submit" value="Confirmar" style="margin-top: 1%">
      			<a href="../cadastros/onibus.php" class="button button-primary">Voltar</a>  <!-- Botão para voltar para o cadastro -->

        	</form>
		</div>

	</body>
</html>