<?php
session_start();
include("../conecta.php"); // Inclui o arquivo de conexão com o banco de dados
$nome = mysqli_real_escape_string($conexao, $_POST['nome']); // Pega a variável que vem do formulário HTML
$id = mysqli_real_escape_string($conexao, $_POST['id_usuario']); // Pega a variável que vem do formulário HTML

$sql = "select * from usuarios where nome_usuario = '$nome' and user_id = '$id';";  // Instrução para confirmar a existência do usuário
$query = mysqli_query($conexao, $sql);  // Executa a instrução
$row = mysqli_num_rows($query); // Pega a quantidade de linhas retornadas pela instrução

if ($row == 0)  // Caso retorne 0 o percurso não existe
{
  echo '<h1>Usuário Não Encontrado</h1>';
  header('refresh:2;url=usuario.php');
  exit();
}

else
{
  $_SESSION['user_id'] = $id;
}

?>


<!DOCTYPE html>
<html lang="pt-br">

  <head>
      <meta charset="UTF-8">
      <title>Alterar Usuários</title>
      <link rel="stylesheet" type="text/css" href="../framework/css/skeleton.css">  <!-- Chama o css do framework -->
      <link rel="stylesheet" type="text/css" href="../framework/css/normalize.css">  <!-- Chama o css do framework -->
      <link rel="stylesheet" href="../painel.css"> <!-- Chama o css da barra de navegação superior -->
  </head>

  <body style="padding: 1%">
    
    <?php while($dado = $query->fetch_array()) {?>
    <h1 style="margin: 1%">Alterar Usuario</h1>
     

  	<form style="margin: 1%;" method = "POST" action="alt_usuarios.php"> <!-- Formulário de cadastro -->
      <div class="row">
        <div class="twelve columns"> <!-- Da o tamanho do grid 12 do framework -->
              
          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Nome Completo:</label> 
            <input type="text" name="nome" class="u-full-width" value="<?php echo$dado["nome_usuario"] ?>">
          </div>

          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">User ID:</label> 
            <input type="text" name="user_id" class="u-full-width" value="<?php echo$dado["user_id"] ?>">
          </div>
              
        </div>

        <div class="twelve columns"> <!-- Da o tamanho do grid 12 do framework -->

          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label>Telefone:</label>
            <input type="text" name="telefone" class="u-full-width" value="<?php echo$dado["telefone_contato"] ?>">
          </div>

        	<div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Tipo:</label> 
            <input type="text" name="tipo" class="u-full-width" value="<?php echo$dado["tipo"] ?>">
          </div>

        </div>

      </div>
      <input class="button-primary" type="submit" value="Alterar" style="margin: 1%"> <!-- botão para enviar o cadastro-->
      <a href="../painel.php" class="button button-primary">Voltar</a>  <!-- Botão para voltar ao painel -->

    </form><?php } ?>
  </body>
</html>