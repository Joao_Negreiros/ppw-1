<?php   
// Indentifica se o login foi efetuado 
session_start();
if (!($_SESSION['usuario']))
{
  header('Location: ../index.php');
}

?>


<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<title>Alterar Passageiro</title>
    <link rel="stylesheet" type="text/css" href="../framework/css/skeleton.css"> <!-- Chama o arquivo css do framework -->
    <link rel="stylesheet" type="text/css" href="../framework/css/normalize.css"> <!-- Chama o arquivo css do framework -->
	</head>
	<body style="padding: 1%">

    <div class="twelve columns"> <!-- Da o tamanho do grid 12 do framework -->
		
		  <h1>Alterar Passageiros</h1>

      <form action="passageiro.php" method="POST"> <!-- Formulário de consulta -->

        	
        <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
          <label for="exampleEmailInput">Nome do Passageiro:</label> 
          <input type="text" name="nome" class="u-full-width">
        </div>

        <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
          <label for="exampleEmailInput">CPF do Passageiro:</label> 
          <input type="text" class="u-full-width" name="cpf_passageiro">
        </div>
             
      	<input class="button-primary" type="submit" value="Confirmar" style="margin-top: 1%">
      	<a href="../cadastros/passageiros.php" class="button button-primary">Voltar</a>  <!-- Botão para voltar para o cadastro -->

      </form>
		</div>

	</body>
</html>