<?php   
// Indentifica se o login foi efetuado 
session_start();
if (!($_SESSION['usuario']))
{
	header('Location: ../index.php');
}

?>


<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<title>Alterar Percurso</title>
	    <link rel="stylesheet" type="text/css" href="../framework/css/skeleton.css"> <!-- Chama o arquivo css do framework -->
      	<link rel="stylesheet" type="text/css" href="../framework/css/normalize.css"> <!-- Chama o arquivo css do framework -->
	</head>
	<body style="padding: 1%">

    	<div class="twelve columns"> <!-- Da o tamanho do grid 12 do framework -->
		
			<h1>Alterar Percurso</h1>

        	<form action="percurso.php" method="POST">  <!-- Formulário de consulta -->
        	
            	<label for="exampleEmailInput">Rota do Percurso:</label> 
            	<input type="text" name="rota" class="u-full-width">
             
      			<input class="button-primary" type="submit" value="Confirmar" style="margin-top: 1%"> <!-- botão para enviar o cadastro-->
      		  	<a href="../cadastros/percurso.php" class="button button-primary">Voltar</a>  <!-- Botão para voltar para o cadastro -->

        	</form>
		</div>

	</body>
</html>