<?php   
include("../conecta.php");  // Inclui o arquivo de conexão com o banco de dados
$placa = mysqli_real_escape_string($conexao, $_POST['placa']); // Pega a variável que vem do formulário HTML

$sql = "select * from onibus where placa = '$placa';"; // Instrução para confirmar a existência do ônibus
$query = mysqli_query($conexao, $sql); // Executa a instrução
$row = mysqli_num_rows($query); // Pega a quantidade de linhas retornadas pela instrução

if ($row == 0) // Caso retorne 0 o ônibus não existe
{
  echo '<h1>Ônibus Não Encontrado</h1>';
  header('refresh:2;url=onibus.php');
  exit();
}

?>

<!DOCTYPE html>
<html lang="pt-br">

  <head>
      <meta charset="UTF-8">
      <title>Alterar Ônibus</title>
      <link rel="stylesheet" type="text/css" href="../framework/css/skeleton.css">  <!-- Chama o css do framework -->
      <link rel="stylesheet" type="text/css" href="../framework/css/normalize.css">  <!-- Chama o css do framework -->
      <link rel="stylesheet" href="../painel.css"> <!-- Chama o css da barra de navegação superior -->
  </head>
  
  <body style="padding: 1%">
    
    <h1 style="margin: 1%">Alterar Ônibus</h1> <?php while($dado = $query->fetch_array()) { ?>

  	<form style="margin: 1%;" method = "POST" action="alt_onibus.php">   <!-- Formulário de cadastro -->

      <div class="row">

        <div class="twelve columns"> <!-- Da o tamanho do grid 12 do framework -->
              
          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Placa:</label>
            <input type="text" name="placa" class="u-full-width" value="<?php echo$dado["placa"] ?>">
          </div>

          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Marca:</label>
            <input type="text" class="u-full-width" name = "marca" value="<?php echo$dado["marca"] ?>">
          </div>

        </div>

        <div class="twelve columns"> <!-- Da o tamanho do grid 12 do framework -->

          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label>Tipo do Veículo:</label>
            <input type="text" name="tipo_veiculo" class="u-full-width" value="<?php echo$dado["tipo_veiculo"] ?>">
          </div>

          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Ano de Fabricação:</label>
            <input type="text" name="fabricação" class="u-full-width" value="<?php echo$dado["ano_fabricacao"] ?>">
          </div>

        </div>

        <div class="twelve columns">
               	
          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Obsevação:</label>
            <input type="text" name="obs" class="u-full-width" value="<?php echo$dado["observacao"] ?>">
          </div>

          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Número de Assentos:</label>
            <input type="text" name="n_assentos" class="u-full-width" value="<?php echo$dado["numero_assentos"] ?>">
          </div>

        </div>

        </div>

      <input class="button-primary" type="submit" value="Alterar" style="margin-top: 1%"> <!-- botão para enviar o cadastro-->
      <a href="../cadastros/onibus.php" class="button button-primary">Voltar</a>  <!-- Botão para voltar ao painel -->

    </form><?php } ?>

  </body>
</html>