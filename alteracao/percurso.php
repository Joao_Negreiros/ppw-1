<?php
include("../conecta.php"); // Inclui o arquivo de conexão com o banco de dados
$rota = mysqli_real_escape_string($conexao, $_POST['rota']); // Pega a variável que vem do formulário HTML

$sql = "select * from percurso where rota = '$rota';";  // Instrução para confirmar a existência do percurso
$query = mysqli_query($conexao, $sql);  // Executa a instrução
$row = mysqli_num_rows($query);  // Pega a quantidade de linhas retornadas pela instrução

if ($row == 0)  // Caso retorne 0 o percurso não existe
{
  echo '<h1>Percurso Não Encontrado</h1>';
  header('refresh:2;url=percurso.php');
  exit();
}


?>

<!DOCTYPE html>
<html lang="pt-br">

  <head>
      <meta charset="UTF-8">
      <title>Cadastrar Percurso</title>
      <link rel="stylesheet" type="text/css" href="../framework/css/skeleton.css">  <!-- Chama o css do framework -->
      <link rel="stylesheet" type="text/css" href="../framework/css/normalize.css">  <!-- Chama o css do framework -->
  </head>

  <body style="padding: 1%">

    <h1>Alterar Percurso</h1><?php while($dado = $query->fetch_array()) { ?>

    <form action="alt_percurso.php" method="POST">

        <div class="twelve columns"> <!-- Da o tamanho do grid 12 do framework -->
              
          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Rota:</label>
            <input type="text" name="rota" class="u-full-width" value="<?php echo$dado["rota"] ?>">
          </div>

          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Duração do Percurso:</label>
            <input type="text" class="u-full-width" name="duracao" value="<?php echo$dado["duracao"] ?>">
          </div>
              
        </div>
    
        <div class="twelve columns"> <!-- Da o tamanho do grid 12 do framework -->

          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label>Necessária Troca de Motorista:</label>
            <input type="text" name="troca_motorista" class="u-full-width" value="<?php echo$dado["troca_motorista"] ?>">
          </div>

          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Quantidade de Motoristas:</label>
            <input type="text" name="qte_troca" class="u-full-width" value="<?php echo$dado["quantos_motoristas"] ?>">
          </div>

        </div>

        <div class="twelve columns">
               	
          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Liberado para Venda:</label>
            <input type="text" name="venda" class="u-full-width" value="<?php echo$dado["liberado_venda"] ?>">
          </div>

          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Preço da Passagem Normal:</label>
            <input type="text" name="preco_s_bagagem" class="u-full-width" value="<?php echo$dado["valor_passagem_sem_bagagem"] ?>">
          </div>

        </div>

        <div class="twelve columns">
                
          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Preço da Passagem com Bagagem:</label>
            <input type="text" name="preco_c_bagagem" class="u-full-width" value="<?php echo$dado["valor_passagem_com_bagagem"] ?>">
          </div>

        </div>

      </div>
      <input class="button-primary" type="submit" value="Alterar" style="margin: 1%"> <!-- botão para enviar o cadastro-->
      <a href="../painel.php" class="button button-primary">Voltar</a>  <!-- Botão para voltar ao painel -->

    </form><?php } ?>
  </body>
</html>