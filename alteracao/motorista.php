<?php
include("../conecta.php"); // Inclui o arquivo de conexão com o banco de dados
$nome = mysqli_real_escape_string($conexao, $_POST['nome']); // Pega a variável que vem do formulário HTML
$id = mysqli_real_escape_string($conexao, $_POST['id_motorista']); // Pega a variável que vem do formulário HTML

$sql = "select * from motoristas where cod_motorista = '$id' and nome = '$nome' ;"; // Instrução para confirmar a existência do motorista
$query = mysqli_query($conexao, $sql); // Executa a instrução
$row = mysqli_num_rows($query); // Pega a quantidade de linhas retornadas pela instrução

if ($row == 0) // Caso retorne 0 o motorista não existe
{
	echo '<h1>Motorista Não Encontrado</h1>';
	header('refresh:2;url=inc_motorista.php');
	exit();
}


?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<title>Consultar Motorista</title>
		<link rel="stylesheet" href="../framework/css/skeleton.css"> <!-- Chama o arquivo css do framework -->
		<link rel="stylesheet" href="../framework/css/normalize.css"> <!-- Chama o arquivo css do framework -->


	</head>
	<body>
		
		<h1>Alterar Motorista <?php echo $nome; ?></h1>
				

		<?php while($dado = $query->fetch_array()) { ?>  <!-- Exibe os dados vindos do BD na linha/colunas abaixo -->

		<form style="margin: 1%;" method = "POST" action="alt_motorista.php">  <!-- Formulário de cadastro -->

	      <div class="row">

	        <div class="twelve columns"> <!-- Da o tamanho do grid 12 do framework -->
	            
	          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
	              <label for="exampleEmailInput">Código do Motorista:</label>
	              <input type="text" name="cod_motorista" class="u-full-width" value="<?php echo$dado["cod_motorista"] ?>">
	          </div>

	          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
	              <label for="exampleEmailInput">Nome:</label>
	              <input type="text" name="nome" class="u-full-width" value="<?php echo$dado["nome"] ?>">
	          </div>

	        </div>

	        <div class="twelve columns">
	                
	          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
	            <label for="exampleEmailInput">Endereço:</label>
	            <input type="text" class="u-full-width" name="endereco" value="<?php echo$dado["endereco"] ?>">
	          </div>
	                
	          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
	            <label>Número:</label>
	            <input type="text" name="numero" class="u-full-width" value="<?php echo$dado["numero"] ?>">
	          </div>
	            
	        </div>

	        <div class="twelve columns"> <!-- Da o tamanho do grid 12 do framework -->
	                  
	          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
	              <label for="exampleEmailInput">Bairro:</label>
	              <input type="text" name="bairro" class="u-full-width" value="<?php echo$dado["bairro"] ?>">
	          </div>

	          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
	            <label>Cidade:</label>
	            <input type="text" name="cidade" class="u-full-width" value="<?php echo$dado["cidade"] ?>">
	          </div>

	        </div>

	        <div class="twelve columns">
	             	
	          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
	            <label for="exampleEmailInput">Estado:</label>
	            <input type="text" name="estado" class="u-full-width" value="<?php echo$dado["estado"] ?>">
	          </div>

	          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
	            <label for="exampleEmailInput">CEP:</label>
	            <input type="text" name="cep" class="u-full-width" value="<?php echo$dado["cep"] ?>">
	          </div>

	        </div>

	        <div class="twelve columns">
	             	
	          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
	            <label for="exampleEmailInput">Fone:</label> 
	            <input type="text" name="fone" class="u-full-width" value="<?php echo$dado["fone"] ?>">
	          </div>

	          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
	            <label for="exampleEmailInput">Data de Nascimento:</label>
	            <input type="text" name="nascimento" class="u-full-width" value="<?php echo$dado["nascimento"] ?>">
	          </div>

	        </div>

	        <div class="twelve columns">
	             	
	              	<div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
	                  	<label for="exampleEmailInput">Vencimento da CNH:</label> 
	                  	<input type="text" name="vencimento_cnh" class="u-full-width" value="<?php echo$dado["vencimento_cnh"] ?>">
	                </div>

	                <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
	                    <label for="exampleEmailInput">Categoria da CNH:</label>
	                    <input type="text" name="categoria_cnh" class="u-full-width" value="<?php echo$dado["tipo_cnh"] ?>">
	                </div>

	        </div>

	      </div>

	      <input class="button-primary" type="submit" value="Alterar" style="margin-top: 1%"> <!-- Botão para enviar o cadastro-->
	      <a href="../cadastros/motorista.php" class="button button-primary">Voltar</a>  <!-- Botão para voltar ao painel -->

	    </form><?php } ?>

	</body>
</html>