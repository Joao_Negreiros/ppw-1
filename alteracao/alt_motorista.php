<?php
// Indentifica se o login foi efetuado
session_start();
if (!($_SESSION['usuario']))
{
  header('Location: ../index.php');
}

include("../conecta.php");

$cod_motorista = mysqli_real_escape_string($conexao, $_POST['cod_motorista']);
$nome = mysqli_real_escape_string($conexao, $_POST['nome']);
$endereco = mysqli_real_escape_string($conexao, $_POST['endereco']);
$numero = mysqli_real_escape_string($conexao, $_POST['numero']);
$bairro = mysqli_real_escape_string($conexao, $_POST['bairro']);
$cidade = mysqli_real_escape_string($conexao, $_POST['cidade']);
$estado = mysqli_real_escape_string($conexao, $_POST['estado']);
$cep = mysqli_real_escape_string($conexao, $_POST['cep']);
$fone = mysqli_real_escape_string($conexao, $_POST['fone']);
$nascimento = mysqli_real_escape_string($conexao, $_POST['nascimento']);
$vencimento_cnh = mysqli_real_escape_string($conexao, $_POST['vencimento_cnh']);
$categoria_cnh = mysqli_real_escape_string($conexao, $_POST['categoria_cnh']);
$sql = "update motoristas set nome = '$nome', endereco = '$endereco', numero = '$numero', bairro = '$bairro', cidade = '$cidade', estado = '$estado', cep = '$cep', fone = '$fone', nascimento = '$nascimento', vencimento_cnh = '$vencimento_cnh', tipo_cnh = '$categoria_cnh' where cod_motorista = '$cod_motorista';";
$query = mysqli_query($conexao, $sql);

if ($query)
{
	echo "<h1>Alteração bem sucedida</h1>";
	header("refresh:2;url=../cadastros/motorista.php");
}
else
{
	echo "<h1>Não foi possível alterar</h1>";
	header("refresh:2;url=motorista.php");
}
?>