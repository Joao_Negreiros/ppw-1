<!DOCTYPE html>
<html>
<head>
<title>Empresa de Ônibus</title>
<link rel="stylesheet" href="style.css">
</head>
<body>

<div class="header">
  <h1>Empresa de Ônibus</h1>
  <p>Projeto pré TCC da matéria PPW I.</p>
</div>
<ul class="navbar">

  <li><a href="../painel.php">Voltar</a></li>

</ul>

<!-- </div>
 -->

<div class="row">
  <div class="leftcolumn">
    <div class="card">
      <h2>Sobre o Projeto</h2>
      <h5>Empresa de Ônibus</h5>
      <p>Projeto de final de semestre das matérias PPW I (Programação para Web 1) e MDBD (Modelagem de Banco de Dados), o projeto tem o intuito de praticar as habilidades adquiridas no 2º ano do curso técnico de informática para internet e preparação para o trabalho de conclusão de curso (TCC). <br> As técnicas demonstradas nesse projeto são: Modelagem e desenvolvimento de um banco de dados MySQL (Banco, Tabelas, Chaves Primárias, Estrangeiras e Composta, Inserções nas tabelas, Alterações, Exclusões e relátorios), desenvolvimento de sistema com PHP, login e logout, conexão com banco de dados, Checagem, Cadastro, Alteração, Exclusão e Relatórios na base de dados, além de lógica de programação, desenvolvimento de páginas web utilizando HTML, CSS, JavaScript(para impressão) e o framework Skeleton Boilerplate Responisve CSS.</p>
    </div>
  </div>
  <div class="rightcolumn">
    <div class="card">
      <h3>Desenvolvedor do Sistema</h3>
      <div class="fakeimg"><p>João Victor Silva Negreiros</p></div>
    </div>
    <div class="card">
      <h3>Sobre Nós</h3>
      <p>2º ano do curso de informática para internet integrado ao ensino médio na ETEC Professor Elias Miguel Junior em Votorantim</p>
    </div>
  </div>
</div>

<div class="footer">
  <h2>© 2019 - 2º Etim Informática para Internet</h2>
</div>

</body>
</html>
