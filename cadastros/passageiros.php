<?php   
session_start();
// Indentifica se o login foi efetuado
if (!($_SESSION['usuario']))
{
  header('Location: ../index.php');
}

?>

<!DOCTYPE html>
<html lang="pt-br">

  <head>
      <meta charset="UTF-8">
      <title>Cadatrar Passageiros</title>
      <link rel="stylesheet" type="text/css" href="../framework/css/skeleton.css">  <!-- Chama o css do framework -->
      <link rel="stylesheet" type="text/css" href="../framework/css/normalize.css">  <!-- Chama o css do framework -->
      <link rel="stylesheet" href="../painel.css"> <!-- Chama o css da barra de navegação superior -->
  </head>

  <body style="padding: 1%">
    
    <div class="cabecalho"> <!-- Barra de navegação superior -->

      <ul>

        <li><a href="../alteracao/inc_passageiro.php" class="">Alterar</a></li>

        <li><a href="../consultas/passageiros.php" class="">Consultar</a></li>

        <li><a href="../exclusao/passageiros.php" class="">Excluir</a></li>

      </ul>

    </div>

    <h1 style="margin: 1%">Cadastrar Passageiros</h1>
  	<form style="margin: 1%;" method = "POST" action="cad_passageiros.php">   <!-- Formulário de cadastro -->
      <div class="row">

        <div class="twelve columns"> <!-- Da o tamanho do grid 12 do framework -->
              
          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Nome:</label>
            <input type="text" name="nome" class="u-full-width">
          </div>

          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">CPF:</label>
            <input type="text" class="u-full-width" name="cpf" placeholder="XXX.XXX.XXX-XX">
          </div>
              
        </div>

        <div class="twelve columns"> <!-- Da o tamanho do grid 12 do framework -->

          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label>Cidade:</label>
            <input type="text" name="cidade" class="u-full-width">
          </div>

          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Estado:</label>
            <select class="u-full-heigth u-full-width"  id="exampleRecipientInput" name="estado">
              <option value="" selected="selected">Selecione o estado</option> <!-- Radio para estados-->
              <option value="AC">Acre</option>
              <option value="AL">Alagoas</option>
              <option value="AP">Amapá</option>
              <option value="AM">Amazonas</option>
              <option value="BA">Bahia</option>
              <option value="CE">Ceará</option>
              <option value="DF">Distrito Federal</option>
              <option value="ES">Espírito Santo</option>
              <option value="GO">Goiás</option>
              <option value="MA">Maranhão</option>
              <option value="MT">Mato Grosso</option>
              <option value="MS">Mato Grosso do sul</option>
              <option value="MG">Minas Gerais</option>
              <option value="PA">Pará</option>
              <option value="PB">Paraíba</option>
              <option value="PR">Paraná</option>
              <option value="PE">Pernambuco</option>
              <option value="PI">Piauí</option>
              <option value="RJ">Rio de Janeiro</option>
              <option value="RN">Rio Grande do Norte</option>
              <option value="RS">Rio Grande do Sul</option>
              <option value="RO">Rondônia</option>
              <option value="RR">Roraima</option>
              <option value="SC">Santa Catarina</option>
              <option value="SP">São Paulo</option>
              <option value="SE">Sergipe</option>
              <option value="TO">Tocantins</option>
            </select>
          </div>

        </div>

        <div class="twelve columns">
               	
          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Data de Nascimento:</label>
            <input type="text" name="nascimento" class="u-full-width" placeholder="AAAA-MM-DD">
          </div>

          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Nota:</label> 
            <input type="text" name="obs" class="u-full-width" placeholder="Ex.:
 Alergia à dipirona, Síndrome do pânico, etc.">
          </div>

        </div>

      </div>

      <input class="button-primary" type="submit" value="Cadrastrar" style="margin-top: 1%"> <!-- botão para enviar o cadastro-->
      <a href="../painel.php" class="button button-primary">Voltar</a>  <!-- Botão para voltar ao painel -->

    </form>

  </body>
</html>
