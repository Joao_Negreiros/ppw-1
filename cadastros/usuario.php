<?php   
// Indentifica se o login foi efetuado
session_start();
if (!($_SESSION['usuario']))
{
  header('Location: ../index.php');
}

?>

<!DOCTYPE html>
<html lang="pt-br">

  <head>
      <meta charset="UTF-8">
      <title>Cadastrar Usuários</title>
      <link rel="stylesheet" type="text/css" href="../framework/css/skeleton.css">  <!-- Chama o css do framework -->
      <link rel="stylesheet" type="text/css" href="../framework/css/normalize.css">  <!-- Chama o css do framework -->
      <link rel="stylesheet" href="../painel.css"> <!-- Chama o css da barra de navegação superior -->
  </head>

  <body style="padding: 1%">
    
    <div class="cabecalho"> <!-- Barra de navegação superior -->

      <ul>

        <li><a href="../alteracao/inc_usuarios.php" class="">Alterar</a></li>

        <li><a href="../consultas/usuario.php" class="">Consultar</a></li>

        <li><a href="../exclusao/usuario.php" class="">Excluir</a></li>

      </ul>

    </div>

    <h1 style="margin: 1%">Cadastrar Usuários</h1>
     

  	<form style="margin: 1%;" method = "POST" action="cad_usuario.php"> <!-- Formulário de cadastro -->
      <div class="row">
        <div class="twelve columns"> <!-- Da o tamanho do grid 12 do framework -->
              
          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Nome Completo:</label> 
            <input type="text" name="nome" class="u-full-width" placeholder="Sem acentos">
          </div>

          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Senha:</label> 
            <input type="password" class="u-full-width" name="senha">
          </div>
              
        </div>

        <div class="twelve columns"> <!-- Da o tamanho do grid 12 do framework -->

          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label>Telefone:</label>
            <input type="text" name="telefone" class="u-full-width" placeholder="(xx) xxxx-xxxx">
          </div>

        	<div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Tipo:</label> 
            <input type="text" name="tipo" class="u-full-width" placeholder="A - Administrador, G - Gerente, C - Caixa">
          </div>

        </div>

        <div class="twelve columns">
               	
          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Número de Identificação:</label>
            <input type="text" name="user_id" class="u-full-width">
          </div>

        </div>

      </div>
      <input class="button-primary" type="submit" value="Cadrastrar" style="margin-top: 1%"> <!-- botão para enviar o cadastro-->
      <a href="../painel.php" class="button button-primary">Voltar</a>  <!-- Botão para voltar ao painel -->

    </form>
  </body>
</html>