<?php   
session_start();
// Indentifica se o login foi efetuado
if (!($_SESSION['usuario']))
{
  header('Location: ../index.php');
}

?>

<!DOCTYPE html>
<html lang="pt-br">

  <head>
      <meta charset="UTF-8">
      <title>Cadastrar Percurso</title>
      <link rel="stylesheet" type="text/css" href="../framework/css/skeleton.css">  <!-- Chama o css do framework -->
      <link rel="stylesheet" type="text/css" href="../framework/css/normalize.css">  <!-- Chama o css do framework -->
      <link rel="stylesheet" href="../painel.css"> <!-- Chama o css da barra de navegação superior -->
  </head>

  <body style="padding: 1%">
    
    <div class="cabecalho"> <!-- Barra de navegação superior -->

      <ul>

        <li><a href="../alteracao/inc_percurso.php" class="">Alterar</a></li>

        <li><a href="../consultas/percurso.php" class="">Consultar</a></li>

        <li><a href="../exclusao/percurso.php" class="">Excluir</a></li>

      </ul>

    </div>

    <h1 style="margin: 1%">Cadastrar Percurso</h1>
  	<form style="margin: 1%;" method = "POST" action="cad_percurso.php">   <!-- Formulário de cadastro -->

      <div class="row">

        <div class="twelve columns"> <!-- Da o tamanho do grid 12 do framework -->
              
          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Rota:</label>
            <input type="text" name="rota" class="u-full-width" placeholder="Origem – Destino: SOROCABA-MONGAGUÁ">
          </div>

          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Duração do Percurso:</label>
            <input type="text" class="u-full-width" name="duracao">
          </div>
              
        </div>

        <div class="twelve columns"> <!-- Da o tamanho do grid 12 do framework -->

          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label>Necessária Troca de Motorista:</label>
            <input type="text" name="troca_motorista" class="u-full-width" placeholder="S - Sim, N - Não">
          </div>

          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Quantidade de Motoristas:</label>
            <input type="text" name="qte_troca" class="u-full-width">
          </div>

        </div>

        <div class="twelve columns">
               	
          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Liberado para Venda:</label>
            <input type="text" name="venda" class="u-full-width" placeholder="S - Sim, N - Não">
          </div>

          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Preço da Passagem Normal:</label>
            <input type="text" name="preco_s_bagagem" class="u-full-width">
          </div>

        </div>

        <div class="twelve columns">
                
          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Preço da Passagem com Bagagem:</label>
            <input type="text" name="preco_c_bagagem" class="u-full-width">
          </div>

        </div>

      </div>
      <input class="button-primary" type="submit" value="Cadrastrar" style="margin-top: 1%"> <!-- botão para enviar o cadastro-->
      <a href="../painel.php" class="button button-primary">Voltar</a>  <!-- Botão para voltar ao painel -->

    </form>
  </body>
</html>