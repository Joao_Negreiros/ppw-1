<?php
include("../conecta.php");

// Variáveis vindas do formulario html
$cod_motorista = mysqli_real_escape_string($conexao, $_POST['cod_motorista']);
$nome = mysqli_real_escape_string($conexao, $_POST['nome']);
$endereco = mysqli_real_escape_string($conexao, $_POST['endereco']);
$numero = mysqli_real_escape_string($conexao, $_POST['numero']);
$bairro = mysqli_real_escape_string($conexao, $_POST['bairro']);
$cidade = mysqli_real_escape_string($conexao, $_POST['cidade']);
$estado = mysqli_real_escape_string($conexao, $_POST['estado']);
$cep = mysqli_real_escape_string($conexao, $_POST['cep']);
$fone = mysqli_real_escape_string($conexao, $_POST['fone']);
$nascimento = mysqli_real_escape_string($conexao, $_POST['nascimento']);
$vencimento_cnh = mysqli_real_escape_string($conexao, $_POST['vencimento_cnh']);
$categoria_cnh = mysqli_real_escape_string($conexao, $_POST['categoria_cnh']);

$sql = "select count(*) as total from motoristas where cod_motorista = '$cod_motorista';"; // Instrução a ser realizada no banco de dados
$result = mysqli_query($conexao, $sql); // Realiza a instrução no banco de dados
$row = mysqli_fetch_assoc($result); // Obtem uma linha do conjunto de resultados como uma matriz associativa

if ($row['total'] == 1) // Checa se a chave primaria já existe, caso não existe continua o código
{
	 echo "<h1 style = 'text-align: center;'>Motorista já existe</h1>";
	 header('refresh:2;url=motorista.php');
	 exit();
}

$sql = "insert into motoristas value('$cod_motorista', '$nome', '$endereco', '$numero', '$bairro', '$cidade', '$estado', '$cep', '$fone', '$nascimento', '$vencimento_cnh', '$categoria_cnh');"; // Instrução a ser realizada no banco de dados

$query = mysqli_query($conexao, $sql); // Realiza a instrução no banco de dados

if ($query) // Se foi possível realizar executa:
{
	echo "<h4> Motorista Cadastrado </h4>";
}
else // se não foi possível executa:
{
	echo "Não foi possivel cadastrar";
	header('refresh:2;url=motorista.php');
}

$conexao->close();

?>