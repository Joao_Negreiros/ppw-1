<?php   
// Indentifica se o login foi efetuado
session_start();
if (!($_SESSION['usuario']))
{
  header('Location: ../index.php');
}

?>

<!DOCTYPE html>
<html lang="pt-br">

  <head>
      <meta charset="UTF-8">
      <title>Cadastrar Ônibus</title>
      <link rel="stylesheet" type="text/css" href="../framework/css/skeleton.css">  <!-- Chama o css do framework -->
      <link rel="stylesheet" type="text/css" href="../framework/css/normalize.css">  <!-- Chama o css do framework -->
      <link rel="stylesheet" href="../painel.css"> <!-- Chama o css da barra de navegação superior -->
  </head>
  
  <body style="padding: 1%">
    
    <div class="cabecalho"> <!-- Barra de navegação superior -->

      <ul>

        <li><a href="../alteracao/inc_onibus.php" class="">Alterar</a></li>

        <li><a href="../consultas/onibus.php" class="">Consultar</a></li>

        <li><a href="../exclusao/onibus.php" class="">Excluir</a></li>

      </ul>

    </div>

    <h1 style="margin: 1%">Cadastrar Ônibus</h1>

  	<form style="margin: 1%;" method = "POST" action="cad_onibus.php">   <!-- Formulário de cadastro -->

      <div class="row">

        <div class="twelve columns"> <!-- Da o tamanho do grid 12 do framework -->
              
          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Placa:</label>
            <input type="text" name="placa" class="u-full-width" placeholder="XXX-0000">
          </div>

          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Marca:</label>
            <input type="text" class="u-full-width" name = "marca" placeholder="(Mercedes, Volvo, etc.">
          </div>

        </div>

        <div class="twelve columns"> <!-- Da o tamanho do grid 12 do framework -->

          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label>Tipo do Veículo:</label>
            <input type="text" name="tipo_veiculo" class="u-full-width" placeholder="V-iagem, F-retado ou L-eito">
          </div>

          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Ano de Fabricação:</label>
            <input type="text" name="fabricação" class="u-full-width" placeholder="AAAA-MM-DD">
          </div>

        </div>

        <div class="twelve columns">
               	
          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Obsevação:</label>
            <input type="text" name="obs" class="u-full-width" placeholder="Veículo só para viagens internacionais, possui carregador
de celular, banheiro, etc.">
          </div>

          <div class="six columns"> <!-- Da o tamanho do grid 6 do framework -->
            <label for="exampleEmailInput">Número de Assentos:</label>
            <input type="text" name="n_assentos" class="u-full-width">
          </div>

        </div>

        </div>

      <input class="button-primary" type="submit" value="Cadrastrar" style="margin-top: 1%"> <!-- botão para enviar o cadastro-->
      <a href="../painel.php" class="button button-primary">Voltar</a>  <!-- Botão para voltar ao painel -->

    </form>

  </body>
</html>