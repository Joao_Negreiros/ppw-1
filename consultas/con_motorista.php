<?php
include("../conecta.php"); // Inclui o arquivo de conexão com o banco de dados
$nome = mysqli_real_escape_string($conexao, $_POST['nome']); // Pega a variável que vem do formulário HTML
$id = mysqli_real_escape_string($conexao, $_POST['id_motorista']); // Pega a variável que vem do formulário HTML

$sql = "select * from motoristas where cod_motorista = '$id' and nome = '$nome' ;"; // Instrução para confirmar a existência do motorista
$query = mysqli_query($conexao, $sql); // Executa a instrução
$row = mysqli_num_rows($query); // Pega a quantidade de linhas retornadas pela instrução

if ($row == 0) // Caso retorne 0 o motorista não existe
{
	echo '<h1>Motorista Não Encontrado</h1>';
	header('refresh:2;url=motorista.php');
	exit();
}


?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<title>Consultar Motorista</title>
		<link rel="stylesheet" href="../framework/css/skeleton.css"> <!-- Chama o arquivo css do framework -->
		<link rel="stylesheet" href="../framework/css/normalize.css"> <!-- Chama o arquivo css do framework -->

		<style>
		
		body{
			padding: 3px;
		}

		table, th, td {
  		border: 1px solid black;
  		padding: 3px;
		}

		</style>
	</head>
	<body>
		
		<h1>Dados do Motorista <?php echo $nome; ?></h1>

		<table class="u-full-width"> <!-- u-full-wdth faz com que o elemento preencha todo o espaço -->
			
			<thead> <!-- Tabela que será exibido os dados vindos do banco de dados -->
				
				<tr>
					<td >Código do Motorista</td>
					<td>Nome</td>
					<td>Endereço</td>
					<td>Número</td>
					<td>Bairro</td>
					<td>Cidade</td>
					<td>Estado</td>
					<td>CEP</td>
					<td>Telefone</td>
					<td>Data de Nascimento</td>
					<td>Data de Vencimento da CNH</td>
					<td>Tipo da CNH</td>
				</tr>
				<?php while($dado = $query->fetch_array()) { ?>  <!-- Exibe os dados vindos do BD na linha/colunas abaixo -->

				<tr>
					<td><?php echo$dado["cod_motorista"] ?></td>  <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["nome"] ?></td>  <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["endereco"] ?></td>  <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["numero"] ?></td>  <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["bairro"] ?></td>  <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["cidade"] ?></td>  <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["estado"] ?></td>  <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["cep"] ?></td>  <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["fone"] ?></td>  <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["nascimento"] ?></td>  <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["vencimento_cnh"] ?></td>  <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["tipo_cnh"] ?></td>  <!-- Exibe o elemento que está entre "" -->
				</tr><?php } ?>

			</thead>

		</table>
		
		<a href="../cadastros/motorista.php" class="button button-primary">Voltar</a> <!-- Botão para voltar para a tela de cadastro -->

	</body>
</html>