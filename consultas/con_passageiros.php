<?php
include("../conecta.php");  // Inclui o arquivo de conexão com o banco de dados
$nome = mysqli_real_escape_string($conexao, $_POST['nome']);  // Pega a variável que vem do formulário HTML
$cpf = mysqli_real_escape_string($conexao, $_POST['cpf_passageiro']);  // Pega a variável que vem do formulário HTML

$sql = "select * from passageiros where nome = '$nome' and cpf = '$cpf' ;"; // Instrução para confirmar a existência do passageiro
$query = mysqli_query($conexao, $sql); // Executa a instrução
$row = mysqli_num_rows($query); // Pega a quantidade de linhas retornadas pela instrução

if ($row == 0)  // Caso retorne 0 o passageiro não existe
{
	echo '<h1>Passageiro Não Encontrado</h1>';
	header('refresh:2;url=passageiros.php');
	exit();
}


?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<title></title>
		<link rel="stylesheet" href="../framework/css/skeleton.css"> <!-- Chama o arquivo css do framework -->
		<link rel="stylesheet" href="../framework/css/normalize.css"> <!-- Chama o arquivo css do framework -->

		<style>
		
		body{
			padding: 3px;
		}

		table, th, td {
  		border: 1px solid black;
  		padding: 3px;
		}

		</style>
	</head>
	<body>
		
		<h1>Dados do Passageiro <?php echo $nome; ?></h1>

		<table class="u-full-width"> <!-- u-full-wdth faz com que o elemento preencha todo o espaço --> 

			
			<thead> <!-- Tabela que será exibido os dados vindos do banco de dados -->
				
				<tr>
					<td >CPF do Passageiro</td>
					<td>Nome</td>
					<td>Cidade</td>
					<td>Estado</td>
					<td>Data de Nascimento</td>
					<td>Observação</td>
				</tr>
				<?php while($dado = $query->fetch_array()) { ?> <!-- Exibe os dados vindos do BD na linha/colunas abaixo -->

				<tr>
					<td><?php echo$dado["cpf"] ?></td> <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["nome"] ?></td> <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["cidade"] ?></td> <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["estado"] ?></td> <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["nascimento"] ?></td> <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["observacao"] ?></td> <!-- Exibe o elemento que está entre "" -->

				</tr><?php } ?>

			</thead>

		</table>
		
		<a href="../cadastros/passageiros.php" class="button button-primary">Voltar</a> <!-- Botão para voltar para a tela de cadastro -->

	</body>
</html>