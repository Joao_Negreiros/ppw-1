<?php
include("../conecta.php");  // Inclui o arquivo de conexão com o banco de dados
$placa = mysqli_real_escape_string($conexao, $_POST['placa']); // Pega a variável que vem do formulário HTML

$sql = "select * from onibus where placa = '$placa';"; // Instrução para confirmar a existência do ônibus
$query = mysqli_query($conexao, $sql); // Executa a instrução
$row = mysqli_num_rows($query); // Pega a quantidade de linhas retornadas pela instrução

if ($row == 0) // Caso retorne 0 o ônibus não existe
{
	echo '<h1>Ônibus Não Encontrado</h1>';
	header('refresh:2;url=onibus.php');
	exit();
}


?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<title>Consultar Ônibus</title>
		<link rel="stylesheet" href="../framework/css/skeleton.css"> <!-- Chama o arquivo css do framework -->
		<link rel="stylesheet" href="../framework/css/normalize.css"> <!-- Chama o arquivo css do framework -->

		<style>
		
		body{
			padding: 3px;
		}

		table, th, td {
  		border: 1px solid black;
  		padding: 3px;
		}

		</style>
	</head>
	<body>
		
		<h1>Dados do Ônibus <?php echo $placa; ?></h1>

		<table class="u-full-width">  <!-- u-full-wdth faz com que o elemento preencha todo o espaço -->
			
			<thead> <!-- Tabela que será exibido os dados vindos do banco de dados -->
				
				<tr>
					<td>Placa</td>
					<td>Marca</td>
					<td>Tipo de Veículo</td>
					<td>Ano de Fabricação</td>
					<td>Observação</td>
					<td>Número de Assentos</td>
				</tr>
				<?php while($dado = $query->fetch_array()) { ?>  <!-- Exibe os dados vindos do BD na linha/colunas abaixo -->

				<tr>
					<td><?php echo$dado["placa"] ?></td> <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["marca"] ?></td> <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["tipo_veiculo"] ?></td>  <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["ano_fabricacao"] ?></td>  <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["observacao"] ?></td>  <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["numero_assentos"] ?></td>  <!-- Exibe o elemento que está entre "" -->
				</tr><?php } ?>

			</thead>

		</table>
		
		<a href="../cadastros/onibus.php" class="button button-primary">Voltar</a>  <!-- Botão para voltar para a tela de cadastro -->

	</body>
</html>