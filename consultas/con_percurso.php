<?php
include("../conecta.php"); // Inclui o arquivo de conexão com o banco de dados
$rota = mysqli_real_escape_string($conexao, $_POST['rota']); // Pega a variável que vem do formulário HTML

$sql = "select * from percurso where rota = '$rota';";  // Instrução para confirmar a existência do percurso
$query = mysqli_query($conexao, $sql);  // Executa a instrução
$row = mysqli_num_rows($query);  // Pega a quantidade de linhas retornadas pela instrução

if ($row == 0)  // Caso retorne 0 o percurso não existe
{
	echo '<h1>Percurso Não Encontrado</h1>';
	header('refresh:2;url=percurso.php');
	exit();
}


?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<title>Consultar Percurso</title>
		<link rel="stylesheet" href="../framework/css/skeleton.css"> <!-- Chama o arquivo css do framework -->
		<link rel="stylesheet" href="../framework/css/normalize.css"> <!-- Chama o arquivo css do framework -->

		<style>
		
		body{
			padding: 3px;
		}

		table, th, td {
  		border: 1px solid black;
  		padding: 3px;
		}

		</style>
	</head>
	<body>
		
		<h1>Dados do Percurso <?php echo $rota; ?></h1>

		<table class="u-full-width"> <!-- u-full-wdth faz com que o elemento preencha todo o espaço --> 
			
			<thead> <!-- Tabela que será exibido os dados vindos do banco de dados -->
				
				<tr>
					<td>Rota</td>
					<td>Duração</td>
					<td>Necessário Troca de Motorista</td>
					<td>Quantidade de Trocas</td>
					<td>Liberado Para Venda</td>
					<td>Preço da Passagem Normal</td>
					<td>Preço da Passagem com Bagagem</td>
				</tr>
				<?php while($dado = $query->fetch_array()) { ?> <!-- Exibe os dados vindos do BD na linha/colunas abaixo -->

				<tr>
					<td><?php echo$dado["rota"] ?></td>  <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["duracao"] ?></td>  <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["troca_motorista"] ?></td>  <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["quantos_motoristas"] ?></td> <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["liberado_venda"] ?></td> <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["valor_passagem_sem_bagagem"] ?></td>  <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["valor_passagem_com_bagagem"] ?></td>  <!-- Exibe o elemento que está entre "" -->
				</tr><?php } ?>

			</thead>

		</table>
		
		<a href="../cadastros/percurso.php" class="button button-primary">Voltar</a> <!-- Botão para voltar para a tela de cadastro -->

	</body>
</html>