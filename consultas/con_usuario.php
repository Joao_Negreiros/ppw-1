<?php
include("../conecta.php"); // Inclui o arquivo de conexão com o banco de dados
$nome = mysqli_real_escape_string($conexao, $_POST['nome']); // Pega a variável que vem do formulário HTML
$id = mysqli_real_escape_string($conexao, $_POST['id_usuario']); // Pega a variável que vem do formulário HTML

$sql = "select * from usuarios where nome_usuario = '$nome' and user_id = '$id';";  // Instrução para confirmar a existência do usuário
$query = mysqli_query($conexao, $sql);  // Executa a instrução
$row = mysqli_num_rows($query); // Pega a quantidade de linhas retornadas pela instrução

if ($row == 0)  // Caso retorne 0 o percurso não existe
{
	echo '<h1>Usuário Não Encontrado</h1>';
	header('refresh:2;url=usuario.php');
	exit();
}


?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<title>Consultar Usuário</title>
		<link rel="stylesheet" href="../framework/css/skeleton.css"> <!-- Chama o arquivo css do framework -->
		<link rel="stylesheet" href="../framework/css/normalize.css"> <!-- Chama o arquivo css do framework -->

		<style>
		
		body{
			padding: 3px;
		}

		table, th, td {
  		border: 1px solid black;
  		padding: 3px;
		}

		</style>
	</head>
	<body>
		
		<h1>Dados do Usuário <?php echo $nome; ?></h1>

		<table class="u-full-width">  <!-- u-full-wdth faz com que o elemento preencha todo o espaço --> 
			
			<thead> <!-- Tabela que será exibido os dados vindos do banco de dados -->
				
				<tr>
					<td>Nome</td>
					<td>Telefone</td>
					<td>Tipo</td>
					<td>User_ID</td>
 
				</tr>
				<?php while($dado = $query->fetch_array()) { ?> <!-- Exibe os dados vindos do BD na linha/colunas abaixo -->

				<tr>
					<td><?php echo$dado["nome_usuario"] ?></td>   <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["telefone_contato"] ?></td>   <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["tipo"] ?></td>  <!-- Exibe o elemento que está entre "" -->
					<td><?php echo$dado["user_id"] ?></td>   <!-- Exibe o elemento que está entre "" -->
				</tr><?php } ?>

			</thead>

		</table>
		
		<a href="../cadastros/usuario.php" class="button button-primary">Voltar</a>  <!-- Botão para voltar para a tela de cadastro -->
 
	</body>
</html>